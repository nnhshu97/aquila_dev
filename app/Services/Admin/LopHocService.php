<?php

namespace App\Services\Admin;

use App\Models\LopHoc;
use App\Services\Helpers\LopMonHocService;
use Exception;
use DB;

class LopHocService extends BaseService
{
    public function model()
    {
        return LopHoc::class;
    }

    public function getListLopHoc($conditions) 
    {
        // $keyword = escape_like($conditions['search']['value']);
        // $keyword = "";
        // $searchColumns = ['name'];
        // $limit = $conditions['length'];
        // $page = $conditions['start'] / $conditions['length'] + 1;
        // $orderConditions = [];
        // if (!empty($conditions['order'])) {
        //     $orderParams = $conditions['order'];
        //     $orderConditions['column'] = $conditions['columns'][$orderParams[0]['column']]['data'];
        //     $orderConditions['dir'] = $orderParams[0]['dir'];
        // }

        // $query = empty($conditions['order']) ? $this->model->orderBy('id', 'desc') : $this->model;
        // $results = self::search($query, $keyword, $searchColumns, $orderConditions, $limit, $page);

        // return $returnData = [
        //     'recordsFiltered' => $results->total(),
        //     'data' => $results->items()
        // ];
        $data = $this->model->with('giaovien')->get();
            foreach ($data as $key => $value) {
                $checkHocVien[] = LopMonHocService::checkLopHocVien($value);
                $value['ngay_bat_dau'] = date("d-m-Y",strtotime($value['ngay_bat_dau'])); 
                $value['ngay_ket_thuc'] = date("d-m-Y",strtotime($value['ngay_ket_thuc'])); 
                $value['mon_hoc_id'] = $value->monhoc->mon_hoc;
            }
        return response()->json([
            'data' => $data
        ]);
    }

    public function store($request)
    { 

        return $this->create($request->all());
    }

    public function diem($hocvien)
    {
       foreach($hocvien as $hocvien){
            return $hocvien->pivot->diem;
            // $hocvien['note'] = $hocvien->pivot->note;
            
       }
    }
}

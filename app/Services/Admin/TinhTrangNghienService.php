<?php

namespace App\Services\Admin;

use App\Models\TinhTrangNghien;
use App\Models\HocVien;
// use App\Services\Helpers\ImageService;
use Exception;
use DB;
use Auth;

class TinhTrangNghienService extends BaseService
{
    public function model()
    {
        return TinhTrangNghien::class;
    }

    public function getListTinhTrangNghien($conditions) 
    {
        if(Auth::guard('admin')->user()->role == 1){
            $hocvienAll = HocVien::all();
        }else{
            if(Auth::guard('admin')->user()->gender == 1 ){
                $hocvienAll = HocVien::where('gioi_tinh','Nam')->with('nguoibaoho')->get();
            }else{
                $hocvienAll = HocVien::where('gioi_tinh','Nữ')->with('nguoibaoho')->get();
            }
        }
        foreach ($hocvienAll as $key) {
            if($key->tinhtrangnghien == null){
                $key['so_lan'] = "";
            }else{
                $key['so_lan'] = count($key->tinhtrangnghien()->get());
            }
        }
    
        return response()->json([
            'data' => $hocvienAll
        ]);
    }

    public function store($request)
    {
        return $this->create($request);
    }

}
